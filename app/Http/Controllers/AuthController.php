<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }

    public function kirim(Request $request){
        //  dd($request->all());
        $fname = $request->fname;
        $lname = $request->lname;
        $biodata = $request->bio;

        return view('welcome2', compact('fname' , 'lname' , 'biodata'));
    }
}
