<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/register', 'AuthController@form');
Route::post('/welcome2', 'AuthController@kirim');

Route::get('/table', function () {
    return view('table');
});

Route::get('/data-tables', function () {
    return view('datatables');
});

// Route::get('/master', function(){
//     return view('layout.master');
// });
